const express = require('express');
const path = require('path');
const shortid = require('shortid');
const cors = require('cors');
const Razorpay = require('razorpay');
const dotenv = require("dotenv");
dotenv.config();

const app = express();


app.use(cors());

const razorpay = new Razorpay({ 
    key_id: process.env.RAZOR_PAY_KEY,
    key_secret: process.env.RAZOR_PAY_KEY_SECRET,
})



app.get('/', (req,res) => {
    res.json({success:'home page'})
})

app.get('/test.jpg', (req, res) => {
    res.sendFile(path.join(__dirname,'test.jpg'));
})


app.post('/razorpay',async (req, res)=>{

    const payment_capture = 1
    const amount = 5
    const currency = 'INR'

    const options = { 
        amount: (amount*100).toString(),
        currency,
        receipt: shortid.generate(),
        payment_capture
    }

    try{
        const response = await razorpay.orders.create(options)
        console.log(response)
        res.status(200).json({
            id:response.id,
            currency: response.currency,
            amount: response.amount,
        })
    }catch(e){
        console.log(e.message)
    }
})


app.listen(1339,()=>{
    console.log('server is up');
})