import logo from './logo.svg';
import './App.css';


const loadScript = (src)=>{
  return new Promise(resolve => {
    const script = document.createElement('script');
    script.src = src
    script.onload = ()=>{
      resolve(true);
    }

    script.onerror = ()=>{
      resolve(false);
    }

    document.body.appendChild(script)

  })
}

const Development = document.domain === "localhost"


function App() {

  const viewRazorPay = async () =>{

    const res = await loadScript('https://checkout.razorpay.com/v1/checkout.js')

    if(!res){
      alert('Payment Failed');
      return
    }

    const data = await fetch('/razorpay',{method: 'POST'}).then(t=> t.json())
    console.log("this is data");
    console.log(data)

    var options = {
      key: Development ? process.env.RAZOR_PAY_KEY:'Prod Key',
      currency:data.currency,
      amount:data.amount.toString(),
      order_id: data.id,
      name: "Tea",
      description: "Test Transaction",
      image: "http://localhost:1339/test.jpg",
      handler: function (response){
          alert(response.razorpay_payment_id);
          alert(response.razorpay_order_id);
          alert(response.razorpay_signature)
      },
      "prefill": {
          "name": "Chandan Chiranjeeb",
      },
  };
  var paymentObject = new window.Razorpay(options);
  paymentObject.open();
  }

  return (
    <>
     <div className="App">
      
        <a className="App-link" onClick={viewRazorPay} target="_blank" rel="noopener noreferrer">
          Pay
        </a>
    </div>
    </>
  );
}

export default App;
